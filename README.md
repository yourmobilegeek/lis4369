> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4369 - Extensible Enterprise Solutions

## Ariana M. Davis 

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install .NET Core
    - Create hwapp application 
    - Create aspnetcoreapp application 
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions


2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Backward - engineer ( using .NET Core ) the following console application
    - Display short assignment requirements
    - Display * your * name as "author"
    - Display current date/time ( must include date/time, your format preference )
    - Must perform and display each mathematical operation


3. [A3 README.md](a3/README.md "My A3 README.md file")
    - This assignment had us create a Future Value Calculator
    - Takes in user input for Rate, First investment, Money added each month, Total time in account


4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create a inheritance program that uses 2 different classes
    - Display current date and time
    - Use both classes to display, edit, and create constructors
    - Perform data validation

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Make appropriate changes to .cshtml and/or .cshtml.cs files
    - Update Index , About and Contact Page

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Backward-engineer (using .NET Core) the console application screenshot
    - Display short assignment requirement and display your name as “author”
    - Display current date/time (must include date/time, your format preference)
    - Must perform and display room size calculations
    - Must include data validation
    - Rounding to two decimal places.
    - Each data member must have get/set methods, also GetArea and GetVolume


6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Prompt user for last name. Return full name , occupation , and age. 
    - Prompt user for age and occupation ( dev or manager ). Retun full name.
    - Allow user to press any key to return back to command line. 



