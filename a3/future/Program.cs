﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
          
      DateTime date = DateTime.Now;

      Console.WriteLine("//////////////////////////////////////////////");
      Console.WriteLine("Program Requirements:\n A3 - Future Value Calculator"); 
      Console.WriteLine("Author: Ariana M. Davis");
      Console.WriteLine("1) Use intrinsic method to display date/time:\n2) Research: What is future value? And, its formula;\n3) Create a FutureValue method using the following parameters: decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep;\n4) Initialize suitable variables(s): use decimal data types for currency variables;\n5) Perform data validation: prompt user until correct data is entered;\n 6) Display money in currency format;\n 7) Allow user to press any key to return back to command line.");
      Console.WriteLine("/////////////////////////////////////////////\nNow: " + date);

      int numYears;
      decimal monthlyDep, yearlyInt, presentValue, futureValue;

      Console.WriteLine("\n");

      bool validBal = false;

      do{   

          Console.WriteLine("Starting Balance: ");
          string userInput = Console.ReadLine();

          validBal = (decimal.TryParse(userInput, out presentValue));

          if(!validBal){
            Console.WriteLine("Starting balance must be numeric.");
          }

         }while(!validBal);

         bool validYear = false;

      do{   

          Console.WriteLine("Term (Years): ");
          string userInput = Console.ReadLine();

          validYear = (int.TryParse(userInput, out numYears));

          if(!validYear){
            Console.WriteLine("Term must be integer data type.");
          }

         }while(!validYear);


      bool validInt = false;


      do{   

          Console.WriteLine("Interest Rate: ");
          string userInput = Console.ReadLine();

          validInt = (decimal.TryParse(userInput, out yearlyInt));

          if(!validInt){
            Console.WriteLine("Interest Rate must be numeric.");
          }

         }while(!validInt);


      bool validDeposit = false;

      do{   

          Console.WriteLine("Monthly Deposit: ");
          string userInput = Console.ReadLine();

          validDeposit = (decimal.TryParse(userInput, out monthlyDep));

          if(!validDeposit){
            Console.WriteLine("Monthly deposit must be numeric.");
          }

         }while(!validDeposit);


        Console.WriteLine("\n\n*** Future Value ***");

        futureValue = FutureValue(presentValue, numYears, yearlyInt, monthlyDep);

        string currency = futureValue.ToString("C");

        Console.WriteLine(currency);

        Console.WriteLine("\nPress any key to exit!");
        Console.ReadKey();

    }

    public static decimal FutureValue(decimal current, int years, decimal rate, decimal payment){

        decimal finalVal = 0;

        rate /= 100;

        double rOverk = ((double)rate/12.0);
        double nTimesk = (years*12);
        decimal afterPower = (decimal)(Math.Pow((1+rOverk),nTimesk));

        decimal firstVal = (current * afterPower);
        decimal secondVal = payment*((afterPower - 1)/(decimal)(rOverk));

        finalVal = firstVal + secondVal;

        return finalVal;

    }

    }
}
