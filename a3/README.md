> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Ariana M. Davis 

### LIS4369 Requirements:

*Three Parts:*

1. Create Final Value Calculator Application
2. Display Date and Time
3. Use Data Validation
4. Research Final Value Formula 

#### README.md file should include the following items:

* Screenshot of running application
* Screenshot of incorrect input

#### Assignment Screenshots:

*Screenshot of correct input*:

![Correct Input Screenshot](img/valid.png)

*Screenshot of wrong input*:

![Wrong Input Screenshot](img/invalid.png)