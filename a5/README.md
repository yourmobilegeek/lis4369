> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Ariana M. Davis 

### LIS4369 Requirements:

*Seven Parts:*

1. Create proper changes to localhost website

#### README.md file should include the following items:

* Screenshot of running application

#### Assignment Screenshots:

*Screenshot of program running*:

![Screenshot](img/pic1.png)

![Screenshot](img/pic2.png)

![Screenshot](img/pic3.png)