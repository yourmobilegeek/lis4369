﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace p2
{
	public class Program {
    	
		public static void Main() {

            string requirements = @"//////////////////////////////////////////////////
            Program Requirements: 
            Using LINQ ( Language Integrated Query )
            Author: Ariana M. Davis 

            *After* completing the required tutorial, create the following program: 

            1) Prompt user for last name. Return full name, occupation and age. 

            2) Prompt user for age and occupation ( Dev or Manager ). Retun full name. 
            (*Must* include data validation on numeric data. ) 

             3) Allow user to press any key to return back to command line.
            //////////////////////////////////////////////////"; 

             Console.WriteLine(requirements);

             Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));

             Console.WriteLine();

        		var people = GenerateListOfPeople();

                Console.WriteLine("*** Finding Items in Collections ***");
                Console.WriteLine("Where: ");
                
                var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
                foreach(var person in peopleOverTheAgeOf30)
                {
                    Console.WriteLine(person.FirstName);
                }

                Console.WriteLine();


                Console.WriteLine("Skip: ");
                IEnumerable<Person> afterTwo = people.Skip(2);
                foreach(var person in afterTwo)
                {
                    Console.WriteLine(person.FirstName);
                }

                Console.WriteLine();


                Console.WriteLine("Take: ");
                IEnumerable<Person> takeTwo = people.Take(2);
                foreach(var person in takeTwo)
                {
                    Console.WriteLine(person.FirstName);
                }

                Console.WriteLine();

                Console.WriteLine("*** Changing Each Item in Collections ***");

                Console.WriteLine("Select: ");
                IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
                foreach(var firstName in allFirstNames)
                {
                    Console.WriteLine(firstName);
                }


                Console.WriteLine();

                Console.WriteLine("Fullname class and objects: ");

                IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
                foreach(var fullName in allFullNames)
                {
                    Console.WriteLine($"{fullName.Last}, {fullName.First}");
                }

                Console.WriteLine();

                Console.WriteLine("*** Finding One Item in Collections ***");

                Console.WriteLine("FirstOrDefault: ");
                Person firstOrDefault = people.FirstOrDefault();
                Console.WriteLine(firstOrDefault.FirstName);

                Console.WriteLine();

                Console.WriteLine("FirstOrDefault as filter: ");

                var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
                var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
                Console.WriteLine(firstThirtyYearOld1.FirstName);
                Console.WriteLine(firstThirtyYearOld2.FirstName);

                Console.WriteLine();

                Console.WriteLine("How orDefault works: ");

                List<Person> emptyList = new List<Person>();
                Person willBeNull = emptyList.FirstOrDefault();

                List<Person> people2 = GenerateListOfPeople();
                Person willAlsoBeNull = people2.FirstOrDefault(x => x.FirstName == "John"); 

                Console.WriteLine(willBeNull == null);
                Console.WriteLine(willAlsoBeNull == null);

                Console.WriteLine();

                Console.WriteLine("LastOrDeafult as filter: ");

                Person lastOrDefault = people.LastOrDefault();
                Console.WriteLine(lastOrDefault.FirstName);
                Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
                Console.WriteLine(lastThirtyYearOld.FirstName);

                Console.WriteLine();

                Console.WriteLine("SingleOrDefault as filter: ");

                Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); 
                Console.WriteLine(single.FirstName);

                Console.WriteLine();

                Console.WriteLine("*** Finding Data About Collections ***");

                Console.WriteLine("Count():");
                int numberOfPeopleInList = people.Count();
                Console.WriteLine(numberOfPeopleInList);

                Console.WriteLine();

                Console.WriteLine("Count() w/ predicate expression:");
                int peopleOverTwentyFive = people.Count(x => x.Age > 25);
                Console.WriteLine(peopleOverTwentyFive);


                Console.WriteLine();

                Console.WriteLine("Any()");

                bool thereArePeople = people.Any();
                Console.WriteLine(thereArePeople);
                bool thereAreNoPeople = emptyList.Any();
                Console.WriteLine(thereAreNoPeople);

                Console.WriteLine();

                Console.WriteLine("All()");

                bool allDevs = people.All(x => x.Occupation == "Dev");
                Console.WriteLine(allDevs);
                bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
                Console.WriteLine(everyoneAtLeastTwentyFour);

                Console.WriteLine('\n');

                Console.WriteLine("*** Converting Results to Collections ***");

                Console.WriteLine("toList(): ");
                List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList();
                IEnumerable<string> devs = listOfDevs.Select(x => x.FirstName);
                foreach(var firstName in devs)
                {
                    Console.WriteLine(firstName);
                }

                Console.WriteLine('\n');

                Console.WriteLine("toArray(): ");

                Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray();
                foreach(var personO in arrayOfDevs)
                {
                    Console.WriteLine(personO.FirstName);
                }

                /* End the tutorial. Begin program requirements */

                Console.WriteLine("*** Required Programs ***\n\n");

                Console.WriteLine("Note: Searches are case sensiive. \n");

                Console.Write("Please enter a last name: ");

                string lastInput = Console.ReadLine();
                int ageInput = 0;

                Console.WriteLine(); 

                var lastNameSearch = people.Where(x => x.LastName == lastInput);
                foreach(var person in lastNameSearch)
                {
                    Console.WriteLine("Matching Criteria: " + person.FirstName + " " + person.LastName + " is a " + person.Occupation + ", and is " + person.Age + " years old.");
                }

                Console.Write("Age: ");
                while (!int.TryParse(Console.ReadLine(), out ageInput))
                {
                    Console.WriteLine("Age must be an integer.");
                }

                Console.WriteLine(); 

                Console.Write("Please Enter Occupation: ");

                Console.WriteLine(); 

                string occInput = Console.ReadLine();

                List<Person> listOfOccupation = people.Where(x => x.Occupation == occInput).ToList();

                var completeNameSearch = listOfOccupation.Where( x=> x.Age == ageInput );
                foreach(var person in completeNameSearch)
                {
                    Console.WriteLine("Matching Criteria: " + person.FirstName + " " + person.LastName + " is a " + person.Occupation + ", and is " + person.Age + " years old.");
                }

                Console.WriteLine(" --- End Program --- \nEnter any key to exit...");

                Console.ReadKey();


    		}

    		public static List<Person> GenerateListOfPeople()
    		{
        		var people = new List<Person>();

        		people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        		people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        		people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        		people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        		people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Manager", Age = 24 });

        		return people;
    		}
	}

	public class Person
	{
	    public string FirstName { get; set; }
	    public string LastName { get; set; }
	    public string Occupation { get; set; }
	    public int Age { get; set; }
	}

    public class FullName
    {
        public string First { get; set; }
        public string Last { get; set; }
    }
}
