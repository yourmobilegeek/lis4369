> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Ariana M. Davis 

### LIS4369 Requirements:

1. Complete LINQ tutorial
2. Create a short program using LINQ statements
3. Display correct data to user

#### README.md file should include the following items:

* Screenshot of room calculator application

#### Assignment Screenshots:

*Screenshot of calculator application*:

![LINQ Screenshot](img/image_1.png)
![LINQ Screenshot](img/image_2.png)

