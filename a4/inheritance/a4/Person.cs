using System;

namespace a4
{
  public class Person
  {

    //data member
    protected string fname; 
    protected string lname; 
    protected int age; 

    //default constructor
    public Person()
    {
        fname = "First name";
        lname = "Last name";
        age = 0; 

        Console.WriteLine("\nCreating base object from default constructor {accepts no arguments}");

        Console.WriteLine("\nCreating " + this.fname + " " + this.lname + " person object from default constructor (accepts no arguements): "); 
    }

    //parameterized constructor
    public Person(string fn="" , string ln="", int a=0)
    {
      fname = fn;
      lname = ln; 
      age = a; 

      Console.WriteLine("\nCreating base object from parameterized constructor {accepts arguments}");

    }

    //*****mutator methods*****
    //setter methods
    public void SetFname(string fn="")
    {
        fname = fn; 
    }

    //setter methods
    public void SetLname(string ln="")
    {
        lname = ln; 
    }

    //setter methods
    public void SetAge(int a=0)
    {
        age = a; 
    }

    //*****accessor methods*****
    //getter methods
    public string GetFname()
    {
        return fname; 
    }

    //getter methods
    public string GetLname()
    {
        return lname; 
    }

    //getter methods
    public int GetAge()
    {
        return age; 
    }

    public virtual string GetObjectInfo()
   {
      return fname + " " + lname + " " + "is " + age.ToString(); 
   }

   public virtual string GetObjectInfo(int x)
   {
      return fname + " " + lname + " " + "is " + age.ToString(); 
   }

  }
}