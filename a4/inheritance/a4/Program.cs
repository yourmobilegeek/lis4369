﻿using System;

namespace a4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string requirements = @"//////////////////////////////////////////////////
            Program Requirements: 
            Using Class Inheritance
            Author: Ariana M. Davis 

            1) Create Person class (must be stored in discrete file apart from Main() method) 

                A) Create three protected data members: 
                1. fname 
                2. lname
                3. age

                B) Create three setter / mutator methods 
                1. SetFname
                2. SetLname
                3. SetAge

                C) Create four getter / accessor methods
                1. GetFname 
                2. GetLname 
                3. GetAge
                4. GetObjectInfo(arg) (virtual method (returns string): allows derived class to override base class method) 
                
                NOTE: For this exercise, do *not* use shorthand/shortcut get/set methods. 

                D) Create two constructors: 
                1. default constructor ( accetps no arguments )
                2. parameterized consturcutor that accepts three arguments 

                E) Instantiate two person objects: 
                a. one from default constructor (display default values, then modify and display new data member values)
                b. one from parametereized constructor passing two arguments to its constructor's parameters), display data member values

            2) Create Student class (must be stored in discrete file apart from Main() method) 

                A) Create three private data members: 
                1. college 
                2. major
                3. gpa

                B) Create three getter / accessor methods 
                1. GetName
                2. GetFullName
                3. GetObjectInfo( demostrates polymorphism: one interface, multiple purposes or roles)

                NOTE: For this exercise, do *not* use shorthand/shortcut get/set methods. 

                C) Create two constructors: 
                1. default constructor ( accetps no arguments )
                2. parameterized consturcutor that accepts six arguments 

                D) Instantiate two student objects: 
                a. one from default constructor (display default values, then modify and display new data member values)
                b. one from parametereized constructor (pass six user-enteredarguments to constructor's parameters), display data member values

             3) Allow user to press any key to return back to command line.
            //////////////////////////////////////////////////"; 

             Console.WriteLine(requirements);

             Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));

             Console.WriteLine();

             Person person1 = new Person(); 

             Console.Write("First Name: "); 
             Console.WriteLine(person1.GetFname());

             Console.Write("Last Name: "); 
             Console.WriteLine(person1.GetLname());

             Console.Write("Age: "); 
             Console.WriteLine(person1.GetAge());

             Console.WriteLine();

             Console.WriteLine("Modify person object's data member values created from default construtor: "); //ask for user input, set it to Person objects
             Console.WriteLine("Use setter/getter methods:");

             Console.Write("First Name: "); 
             string p_fname = Console.ReadLine();

             Console.Write("Last Name: "); 
             string p_lname = Console.ReadLine();

             int p_age = 0;

             Console.Write("Age: ");
             while (!int.TryParse(Console.ReadLine(), out p_age))
             {
                Console.Write("Age must be integer.");
             }

             person1.SetFname(p_fname); 
             person1.SetLname(p_lname);
             person1.SetAge(p_age); 

             Console.WriteLine("\nDisplay object's new data member values:");
             Console.Write("First Name: ");
             Console.WriteLine(person1.GetFname());

             Console.Write("Last Name: "); 
             Console.WriteLine(person1.GetLname());

             Console.Write("Age: "); 
             Console.WriteLine(person1.GetAge());

             Console.WriteLine();

             Console.WriteLine("Call parameterized constructor (accept two arguments):");
            
             Console.Write("First Name: ");
             p_fname = Console.ReadLine();

             Console.Write("Last Name: ");
             p_lname = Console.ReadLine();

             Console.Write("Age: ");
             while (!int.TryParse(Console.ReadLine(), out p_age))
             {
                Console.Write("Age must be integer.");
             }

             Console.WriteLine();

             Person person2 = new Person(p_fname, p_lname, p_age); 

             Console.Write("First Name: "); 
             Console.WriteLine(person2.GetFname());

             Console.Write("Last Name: "); 
             Console.WriteLine(person2.GetLname());

             Console.Write("Age: "); 
             Console.WriteLine(person2.GetAge());

             Console.WriteLine("\nCall derived default constructor (inherits from base class): ");
             Console.WriteLine("***Note***: Because derived default student constructor does not call\n" + "base class constructor explicitly, default constructor in base class called implicitly");
             Console.WriteLine("\nThat is why, here, base class *must* contain default constructor!");

             Student student1 = new Student(); 

             Console.Write("First Name: "); 
             Console.WriteLine(student1.GetFname());

             Console.Write("Last Name: "); 
             Console.WriteLine(student1.GetLname());

             Console.Write("Age: "); 
             Console.WriteLine(student1.GetAge());

             Console.WriteLine("\nDemonstrating Polymorphism (new derived object):");
             Console.WriteLine("(Calling parameterized base class constructor explicitly.)\n");

             Console.Write("First Name: "); 
             string s_fname = Console.ReadLine();

             Console.Write("Last Name: "); 
             string s_lname = Console.ReadLine();

             int s_age = 0; 

             Console.Write("Age: ");
             while (!int.TryParse(Console.ReadLine(), out s_age))
             {
                Console.Write("Age must be integer.");
             }

             Console.Write("College: "); 
             string s_college = Console.ReadLine();

             Console.Write("Major: "); 
             string s_major = Console.ReadLine();

             double s_gpa = 0.0;

             Console.Write("GPA: ");
             while (!double.TryParse(Console.ReadLine(), out s_gpa))
             {
                Console.Write("GPA must be numeric.");
             }

             Console.WriteLine(); 

             Student student2 = new Student(s_fname, s_lname, s_age, s_college, s_major, s_gpa); 

             Console.WriteLine(); 

             Console.Write("person2 - GetObjectInfo (virtual): \n"); 
             Console.WriteLine(person2.GetObjectInfo());

             Console.WriteLine(); 

             Console.Write("student2 - GetObjectInfo (overridden): \n"); 
             Console.WriteLine(student2.GetObjectInfo());

             Console.WriteLine(); 
             Console.WriteLine("\nPress any key to exit!");
             Console.ReadKey();

        }

    }
}
