> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Ariana M. Davis 

### LIS4369 Requirements:

*Seven Parts:*

1. Backward-engineer (using .NET Core) the following console application screenshot:
2. Requirements (see below screenshots):
3. Display short assignment requirements
4. Display name as author
5. Display current date/time (must include date/time, your format preference).
6. Create two classes: person and student (see fields and methods below).
7. Must include data validation on numeric data. 

#### README.md file should include the following items:

* Screenshot of running application

#### Assignment Screenshots:

*Screenshot of program running*:

![Screenshot](img/pic1.png)

![Screenshot](img/pic2.png)